module.exports = {
    title: 'Expresso Framework',
    tagline: 'Express + TypeORM + Preact + TypeScript = ♥',
    url: 'https://expresso.evan.guru',
    baseUrl: '/',
    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'warn',
    favicon: 'img/favicon.ico',
    organizationName: 'Expresso Framework', // Usually your GitHub org/user name.
    projectName: 'expresso', // Usually your repo name.
    themeConfig: {
        navbar: {
            title: 'Expresso',
            logo: {
                alt: 'Expresso Logo',
                src: 'img/logo.svg',
            },
            items: [
                {
                    to: 'docs/',
                    activeBasePath: 'docs',
                    label: 'Docs',
                    position: 'left',
                },
                {
                    href: 'https://gitlab.com/Evan_Darwin/expresso',
                    label: 'Source Code',
                },
                {to: 'blog', label: 'Blog', position: 'left'},
            ],
        },
        footer: {
            style: 'dark',
            links: [
                {
                    title: 'Docs',
                    items: [
                        {
                            label: 'Introduction',
                            to: 'docs/',
                        },
                        {
                            label: 'Getting Started',
                            to: 'docs/get-started',
                        },
                        {
                            label: 'Features',
                            to: 'docs/features/'
                        }
                    ],
                },
                {
                    title: 'Community',
                    items: [
                        {
                            label: 'Example App',
                            href: 'https://gitlab.com/Evan_Darwin/expresso-example',
                        },
                        {
                            label: 'Issues',
                            href: 'https://gitlab.com/Evan_Darwin/expresso/-/issues',
                        },
                    ],
                },
                {
                    title: 'More',
                    items: [
                        {
                            label: 'Blog',
                            to: 'blog',
                        },
                        {
                            label: 'GitLab',
                            href: 'https://gitlab.com/Evan_Darwin/expresso-example',
                        },
                    ],
                },
            ],
            copyright: `Copyright © ${new Date().getFullYear()} - Expresso Framework / Evan Darwin`,
        },
    },
    presets: [
        [
            '@docusaurus/preset-classic',
            {
                docs: {
                    sidebarPath: require.resolve('./sidebars.js'),
                    // Please change this to your repo.
                    editUrl: 'https://gitlab.com/Evan_Darwin/expresso-docs/-/edit/master/',
                },
                blog: {
                    showReadingTime: true,
                    // Please change this to your repo.
                    editUrl: 'https://gitlab.com/Evan_Darwin/expresso-docs/-/edit/master/',
                },
                theme: {
                    customCss: require.resolve('./src/css/custom.css'),
                },
            },
        ],
    ],
};
