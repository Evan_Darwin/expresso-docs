---
id: expresso-cli
title: Expresso CLI
sidebar_label: Expresso CLI Utility
slug: /cli
---

## Command Line Utility

After installing expresso in your project, the `expresso` command will become available.

![Expresso Command Line Utility](/img/example_cli.png)

### db:test
Test the current configuration of your database, and ensure that it is able to connect properly, and necessary drivers
are available and installed.

### db:sync
Synchronize your database tables with the current schema _in code_. Running this when the application is not in debug mode
will trigger a warning prompt, as migrations should be used instead.

### compile:sass
Compile the SASS file at `scss/app.scss` into a minified CSS file in the static directory.
