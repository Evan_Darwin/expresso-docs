---
id: expresso-intro
title: Expresso Introduction
sidebar_label: Introduction
slug: /
---

:::warning
Expresso is currently under _heavy development_. APIs may change with short notice, and refactoring may be
required between updates for **0.1.x**.
:::

Welcome to the **Expresso Framework**! Expresso is a wrapper around express, which aims to:
 * Reduce redundancy between **express** projects, without sacrificing any flexibility
 * Ensure code can be **fully typed** with TypeScript
 * Promote basic **application security** and defaults
 * Improve **async** support within **express**

-----

# The Framework
  * **[Express](https://expressjs.com/) - The same great express syntax and flexibility**
  * **[Preact](https://preactjs.com/) - All the power of React in 10KB**
  * **[TypeScript 4.1](https://www.typescriptlang.org/) - A powerful superset of JavaScript**
  * **[TypeORM](https://typeorm.io/)** _(optional)_ **- A powerful ORM with support for a wide variety of databases**
    * Supports: _MySQL_, _MariaDB_, _PostgreSQL_, _Oracle_, _MSSQL_, _MongoDB_, _Sql.js_, _CockroachDB_, and _SQLite_
  * **[TSLog](https://tslog.js.org/#/) - A beautiful TypeScript logger**

## Features
  * **Easily render JSX and XML server-side**
  * **Asynchronous express routes**
  * **Automatic request IDs**
  * **Minimal overhead / dependencies**
  * **Easy 404 & 500 error pages**


