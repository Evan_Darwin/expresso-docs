---
id: features-application
title: Application
sidebar_label: Application
slug: /features/
---

:::info
## Express Documentation
For more information about **Application**, [view the express documentation](https://expressjs.com/en/4x/api.html#app).
:::

## Create a New Application

You can create an instance of **expresso** like so:

```ts
import expresso from "expresso";
const app = expresso();
```

This `app` instance is an express application, that provides all of the utilities you may be familiar with.


-----
## Features

### async Request Handlers

**Expresso** supports **asynchronous request handlers** in route definitions.

This means you can finally use `async` in request handlers, _without needing to wrap every method in a try/catch_.

```jsx
app.post<{id: string}>("/process/:id", async (req, res) => {
    const user = await User.findOneOrFail(+req.params.id);
    res.send(<p>Welcome {user.name}</p>);
});
```

If an exception is encountered, then it will be routed through the express error handler **automatically**.

That's all there is to it!

### Environment Variable Parsing
For more information about automatic **environment variable parsing**, view it's [documentation subpage](#).
Environment keys provided in the configuration will be available in typings for [`app.env()`](#appenv)


-----
## Properties

### app.debug
A simple getter property for determining if the application is running in debug mode. No more trying to import
constants from various files.

```jsx
console.log(app.debug)
// returns: false
```

### app.env()
Retrieve values from the environment, with the option to provide defaults. This method is fully typed.

```jsx
app.env('IS_CUSTOM_VALUE', false)
// return type is always boolean
```

### app.logger
A built-in logging system powered by [tslog](https://tslog.js.org/#/) that can be easily accessed in any context.

```jsx
app.logger.warn("Ruh roh!")
```

This same instance is also available as [`req.logger`](#).



