---
id: features-middleware
title: Middleware
sidebar_label: Middleware
slug: /features/middleware
---

:::info
## Express Documentation
For more information about **Middleware**, [view the express documentation](https://expressjs.com/en/guide/using-middleware.html).
:::

Expresso makes the most common middleware and parsers available, without needing to install various middleware dependencies and types.

## Middleware Import

In order to access these middlewares, you will want to import the **middleware** object from **expresso**.

```jsx
import { middleware } from "expresso";
```

-----
## Common Middleware
### .static()
You can expose a directory in your project to be used for static assets with:

```jsx
app.use(middleware.static('./static'))
```

### .notFound()

You can easily register the expresso default 404 page, which allows for easy debugging of routing issues.

:::info
The 404 route must be registered **last**, or it may be rendered before your intended routes.
:::

#### Default 404 page
```jsx
app.use(middleware.notFound());
```

#### Rendering a custom page
```jsx
app.use(middleware.notFound((req, res, next) => {
    res.send(<Custom404Page />);
}));
```

### .error()
You can also register a default error handler, provided by expresso:

#### Default error page
```jsx
app.use(middleware.error());
```

#### Rendering a custom error page
```jsx
app.use(middleware.error((err, req, res, next) => {
    res.send(<Custom500Page req={req} error={err} />);
}))
```

### .rid()
A simple implementation of [`connect-rid`](https://expressjs.com/en/resources/middleware/connect-rid.html),
powered by Expresso's `req.uuid`. This middleware will add the request's UUID to the response of each request, with
the specified header name.

```jsx
app.use(middleware.rid())           // Default header is 'X-RID'
app.use(middleware.rid('x-id'))     // Custom header
```

-----
## Body Parsers

The following body parsers allow you to parse various formats of client HTTP data. More information about middleware
in express can be found on their [middleware guide](https://expressjs.com/en/guide/using-middleware.html).

### .urlencoded()
Add support for URL encoded form data submitted to the application. This middleware is recommended if you will be
processing data submitted by common browsers.

```jsx
app.use(middleware.urlencoded())
```

### .json()
Add support for JSON form data submitted to the application.

```jsx
app.use(middleware.json())
```

### .raw()
Process raw request data submitted from clients.

```jsx
app.use(middleware.raw())
```

