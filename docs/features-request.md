---
id: features-request
title: Request
sidebar_label: Request
slug: /features/request
---

:::info
## Express Documentation
For more information about **Request**, [view the express documentation](https://expressjs.com/en/4x/api.html#req).
:::

## Properties

### .at
The `.at` property is defined on expresso request objects, which contains the `Date` at which the request was initiated.


### .logger
A **[tslog](https://github.com/fullstack-build/tslog#readme)** instance for easily logging information within request
handlers. This property is an alias for [`app.logger`](/docs/features/#applogger).


### .msTotal
The `.msTotal` property returns the difference in milliseconds between the current time and when the request was received.
A very useful property for easily assessing performance.

#### Example
```jsx
app.get("/", (req, res) => {
    setTimeout(() => {
        res.send({timeTaken: req.msTotal})
    }, 250);
})
```


### .uuid
A unique UUID can be generated for each request. The UUID is only generated on access, so there is no overhead for
individual requests unless `.uuid` is used. By default, **v4 UUIDs** are returned.

You may also specify a custom generation function for your own unique ID logic.

#### Example
```jsx
console.log(req.uuid)
// returns: 7ed396b0-a60c-406d-a9d9-844f63fe0464
```
