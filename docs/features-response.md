---
id: features-response
title: Response
sidebar_label: Response
slug: /features/response
---

:::info
## Express Documentation
For more information about **Response**, [view the express documentation](https://expressjs.com/en/4x/api.html#res).
:::

:::info
#### Overriding the default JSX renderer
In the case you want to use a different JSX renderer than what is defined in your
`tsconfig.json`, you can specify the `@jsx` comment with the function to use in the
beginning of your file.
:::

## Properties

### .send()
Expresso extends support on `res.send()` with the additional JSX formats:

  * **HTML** via `preact`
  * **XML** via `jsx-xml`

#### JSX Rendering
```jsx
res.send(<YourHomepage />)
```

### .xml()
If you want to render XML with `jsx-xml`, Expresso provides **res.xml()**:

```jsx
/** @jsx JSXXML */
import {JSXXML} from "jsx-xml";

app.get("/", (req, res) => {
  res.send(<animal>
    <type>feline</type>
    <name>Deuteronomy</name>
  </animal>)
});
```
