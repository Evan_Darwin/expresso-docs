---
id: expresso-getting-started
title: Getting Started
sidebar_label: Getting Started
slug: /get-started
---

:::warning
Expresso is currently under _heavy development_. APIs may change with short notice, and refactoring may be
required between updates for **0.1.x**.
:::

-----

## Cloning The Example Project
For new projects, we recommend our example project. You can easily clone the repository, and have a working, ready
expresso application that can be used with any database.

```bash
# clone the example project
git clone https://gitlab.com/Evan_Darwin/expresso-example.git your-project
cd your-project
# reset the git repo
rm -rf .git ; git init
# install dependencies
yarn
```

#### The expresso-example repository is [available here](https://gitlab.com/Evan_Darwin/expresso-example).


-----
## Installing via npm/yarn
To only install the expresso framework, you can install the package like so:

### yarn
```shell
yarn add https://gitlab.com/Evan_Darwin/expresso.git#master
```

### npm
:::info
Unfortunately npm does not support compiling packages cloned from git at the moment. `yarn` is recommended until expresso
migrates to official npm packages.
:::

-----
## Minimum Example
The following code is the recommended minimum for running the expresso framework; however you can do whatever you want, really. 😉

```jsx
(() => {
    const app = expresso();         // create app
    app.use(middleware.helmet())    // secure headers

    // handle browser form data
    app.use(middleware.parsers.urlencoded())

    /** Your routes and other code here */

    app.use(middleware.notFound())  // 404 page
    app.use(middleware.error())     // 500 page

    // listen
    app.listen(3000)
})()
```
