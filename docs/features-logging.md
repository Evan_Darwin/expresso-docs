﻿---
id: features-logging
title: Logging
sidebar_label: Logging
slug: /features/logging
---

The `tslog` package is included in expresso. A logger is automatically made available on `app.logger` and `req.logger`.

:::info
## TSLog Documentation
For more details about how to use tslog independently in your project, view its [documentation here](https://github.com/fullstack-build/tslog#readme).
:::

## Logging Messages & Errors
```ts
// get a child logger
const log = app.logger.getChildLogger({name: "sub-name"});
// available methods
log.silly("I am a silly log.");
log.trace("I am a trace log with a stack trace.");
log.debug("I am a debug log.");
log.info("I am an info log.");
log.warn("I am a warn log with a json object:", {foo: "bar"});
log.error("I am an error log.");
log.fatal(new Error("I am a pretty Error with a stacktrace."));
```
